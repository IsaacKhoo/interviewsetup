import { constant  } from "./Constants.js";

const delayAsync = async (ms = 2000) => {
	return new Promise((res, rej) => setTimeout(() => {
        res();
    }, ms));
};

const functionAsync = async () => {
	try {
        console.log(`entered function`);
        await delayAsync();
        console.log(`exit function`, constant);
	} catch (err) {
		console.error(err);
	}
};
functionAsync();